<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::apiResource('user', 'API\ProfileController');

// //user
// Route::post('/user','API\UserController@store');
// Route::get('/user', 'API\UserController@index');
// Route::get('/user/{id}', 'API\UserController@show');
// Route::delete('/user/{id}', 'API\UserController@destroy');
// Route::put('/user/{id}', 'API\UserController@update');

Route::post('/user', [ProfileController::class, 'store']);
Route::get('/user', [ProfileController::class, 'index']);
Route::get('/user/{id}', [ProfileController::class, 'show']);
Route::delete('/user/{id}', [ProfileController::class, 'destroy']);
Route::put('/user/{id}', [ProfileController::class, 'update']);


