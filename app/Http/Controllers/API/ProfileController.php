<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use Exception;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $User = User::query();
            $pagination = 5;
            $User = $User->orderBy('created_at', 'desc')->paginate($pagination);

            $response = $User;
            $code = 200;
        }catch (Exception $e) {
            $code = 500;
            $response = $e -> getMessage();
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif',
            'date_of_birth' => 'required',
            'age' => 'required'

        ]);

        try{

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $User = new User();

            $User->name = $request->name;
            $User->email = $request->email;
            $User->alamat = $request->alamat;
            $User->image = $imageName;
            $User->date_of_birth = $request->date_of_birth;
            $User->age = $request->age;

            $User->save();

            $code=200;
            $response=$User;

        } catch (Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            } else{
                $code = 500;
                $response=$e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $User = User::findOrFail($id);

            $code = 200;
            $response = $User;
        }catch (\Exception $e){
            if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e -> getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate($request,[
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'date_of_birth' => 'required',
            'age' => 'required'
        ]);

        try{
            $User = User::find($id);

            $User->name = $request->name;
            $User->email = $request->email;
            $User->alamat = $request->alamat;
            $User->date_of_birth = $request->date_of_birth;
            $User->age = $request->age;
          
            $User -> save();
            $code = 200;
            $response = $User;
        }catch(\Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e -> getMessage();
            }
        }
        return apiResponseBuilder($code,$response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $User = User::find($id);
            $User->delete();

            $code = 200;
            $response = $User;
        }catch(\Exception $e){
            $code = 500;
            $response = $e->getMessage(); 
        }
        return apiResponseBuilder($code,$response);

    }
}
